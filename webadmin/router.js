function route(handle, pathname, response, parsedRequest) {
	if (typeof handle[pathname] === 'function') {
		handle[pathname](response, parsedRequest);
	} else {
		console.error("No request handler found for " + pathname);
		response.writeHead(404, {"Content-Type": "text/html"});
		response.write("404 Not found");
		response.end();
	}
}

exports.route = route;