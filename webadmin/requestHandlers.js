var mustache = require('mustache');
var fs = require('fs');
var path = require('path');
var cache = require('memory-cache');

function index(response, parsedRequest) {
	var mails = cache.get('mails');
	var view = {
		"mails": mails,
		"toStr": function() {
			var str = '';
			for (var key in this.to)
				str += JSON.stringify(this.to[key])
			return str;
		},
		"fromStr": function() {
			var str = '';
			for (var key in this.from)
				str += JSON.stringify(this.from[key])
			return str;
		}
	};

	var template = fs.readFileSync(path.resolve('webadmin/templates/index.mustache'), 'utf8');
	var html = mustache.to_html(template, view);

	response.writeHead(200, {"Content-Type": "text/html"});
	response.write(html);
	response.end();
}

function details(response, parsedRequest)
{
	var id = parsedRequest.query['id'];

	var mails = cache.get('mails');
	var mail = mails[id];

	var view = {
		"mail": mail,
		"headersStr": function() {
			var str = '';
			for (var key in mail.headers)
				str += JSON.stringify(mail.headers[key]) + '<br>'
			return str;
		}
	};

	var template = fs.readFileSync(path.resolve('webadmin/templates/details.mustache'), 'utf8');
	var html = mustache.to_html(template, view);

	response.writeHead(200, {"Content-Type": "text/html"});
	response.write(html);
	response.end();
}

exports.index = index;
exports.details = details;