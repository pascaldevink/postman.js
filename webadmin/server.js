var http = require("http");
var url = require("url");

function start(env, route, handle) {
	function onRequest(request, response) {
		var parsedRequest = url.parse(request.url, true);
		var pathname = parsedRequest.pathname;
		route(handle, pathname, response, parsedRequest);
	}

	http.createServer(onRequest).listen(env.webadminPort);
	console.log("Server has started on port " + env.webadminPort);
}

exports.start = start;