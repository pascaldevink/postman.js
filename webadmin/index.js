var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/"] = requestHandlers.index;
handle["/show"] = requestHandlers.details;

exports.start = function(env)
{
	server.start(env, router.route, handle);
}