var cache = require('memory-cache');

exports.help = function(client, from, options)
{
	var helpMessage = 'help            - This help message\n' +
		'last            - Repeats the last incoming message\n' +
		'last <nr>       - Repeats the last X incoming messages\n'

	client.say(from, helpMessage);
}

exports.last = function(client, from, options)
{
	var mails = cache.get('mails');

	if (!mails || mails.length < 1)
	{
		client.say(from, 'No mails found');
		return;
	}

	if (!options || options.length < 1)
	{
		options = new Array();
		options[0] = 1;
	}

	var mail, i;
	for (i = 0; i < options[0]; i++)
	{
		mail = mails[i];
		if (!mail)
			break;

		client.say(from, "Mail from ("+mail.from[0].name+", "+mail.from[0].address+") title ("+mail.subject+") to ("+mail.to[0].name+", "+mail.to[0].address+"):");
		client.say(from, mail.text);
	}

}