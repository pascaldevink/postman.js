exports.command = function (handle, client, from, command, options)
{
	if (typeof handle[command] === 'function')
	{
		handle[command](client, from, options);
	}
	else if (command === "")
	{
		client.say(from, 'You called sir?');
	}
	else
	{
		console.error('No command handler found for ' + command);
		client.say(from, '404 Not found');
	}
}