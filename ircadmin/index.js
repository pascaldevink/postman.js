var irc = require('irc');
var commands = require('./commands');
var commandHandlers = require('./commandHandlers');

var handle = {}
handle["help"] = commandHandlers.help;
handle["last"] = commandHandlers.last;

exports.handle = function(client, message, from)
{
	console.log('Received message "'+message+'" from ' + from);

	var messageParts = message.split(' ');
	message = messageParts.shift();

	commands.command(handle, client, from, message, messageParts);
}

