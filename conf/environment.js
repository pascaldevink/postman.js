module.exports.development =
{
	"ircNetwork": "irc.tweakers.net",
	"ircNickname": "postman-dev",
	"ircChannels": ["#postman-dev"],
	"ircHandle": "!postman ",
	"smtpPort": "8025",
	"webadminPort": "8888"
}

module.exports.production =
{
	"ircNetwork": "irc.tweakers.net",
	"ircNickname": "postman",
	"ircChannels": ["#postman"],
	"ircHandle": "!postman ",
	"smtpPort": "8025",
	"webadminPort": "8088"
}