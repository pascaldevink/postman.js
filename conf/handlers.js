var irc = require('../handlers/irc');
var cache = require('../handlers/cache');

var handlers = {};
handlers['irc']		= irc;
handlers['cache']	= cache;

exports.handlers = handlers;
exports.init = function(env)
{
	// Loop over the handlers and call the start() function
	for (var key in handlers)
	{
		var handler = handlers[key];
		handler.config(env);
		handler.start();
	}
}