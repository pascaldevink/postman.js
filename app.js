var envious = require('envious');
var configuration = require('./conf/environment');
var emaillisten = require("email-listener");
var handlers = require('./conf/handlers');
var webadmin = require('./webadmin/index');

envious.development = configuration.development;
envious.production = configuration.production;
envious.default_env = "development";
var env = envious.apply();

emaillisten.start(env.smtpPort);
handlers.init(env);
webadmin.start(env);

emaillisten.on("msg", function(recipient, rawbody, parsed){
	console.log('Received email for "'+recipient+'", about "'+parsed.subject+'"');

	// Loop over the handlers and call the handle() function
	for (var key in handlers.handlers)
	{
		var handler = handlers.handlers[key];
		handler.handle(recipient, rawbody, parsed);
	}
});
