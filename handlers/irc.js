var irc = require('irc');
var ircAdmin = require('../ircadmin');
var client = null;

var ircNetwork = null;
var ircNickname = null;
var ircChannels = null;
var ircHandle = null;

function config(env)
{
	ircNetwork = env.ircNetwork;
	ircNickname = env.ircNickname;
	ircChannels = env.ircChannels;
	ircHandle = env.ircHandle;
}

function start()
{
	client = new irc.Client(ircNetwork, ircNickname, {
		channels: ircChannels
	});
	console.log('irc handler connected to ' + ircNetwork);

	client.addListener('message', function(from, to, message) {
		var isForMe = message.indexOf(ircHandle);
		if (isForMe != -1)
		{
			message = message.substring(ircHandle.length, message.length);
			ircAdmin.handle(client, message, from);
		}
	});

	client.addListener('pm', function(from, message) {
		ircAdmin.handle(client, message, from);
	});
}

function handle(recipient, rawbody, parsed)
{
	for (var key in ircChannels)
	{
		var channel = ircChannels[key];
		client.say(channel, "Mail from ("+parsed.from[0].name+", "+parsed.from[0].address+") title ("+parsed.subject+") to ("+parsed.to[0].name+", "+parsed.to[0].address+"):");
		client.say(channel, parsed.text);
	}
}

exports.config = config;
exports.start = start;
exports.handle = handle;

