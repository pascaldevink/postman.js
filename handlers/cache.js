var cache = null

function config(env)
{
}

function start()
{
	cache = require('memory-cache');
}

function handle(recipient, rawbody, parsed)
{
	// Storing the mail in the cache
	var mails = cache.get('mails');
	if (mails === null)
		mails = new Array();

	var currentSize = mails.length;
	parsed.id = currentSize;
	mails.unshift(parsed);

	if (currentSize > 5)
		mails.shift();

	cache.put('mails', mails);
}

exports.config = config;
exports.start = start;
exports.handle = handle;